#!/usr/bin/env python

import rospy
from std_msgs.msg import String,Bool
import json
from std_srvs.srv import Trigger, TriggerResponse
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState
import time
import os
import requests
import re
import math
import subprocess

class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)
    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)
    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

class GameController:

    def __init__(self, robot_name):

        self.robot_name = robot_name

        self.correct = 0
        self.Status = "running"
        self.score = 0

        self.time_passed = 0.0
        self.start_time = 0.0
        
        self.finish_area1 = Vector3D(0,-0.05,0.115)
        self.finish_area2 = Vector3D(0,0.12,0.115)

        
        self.model_names = ['lego_block_1','lego_block_2','lego_block_3','lego_block_4','lego_block_5']
        self.check_point = [0,0,0,0,0]
        
        #initialize ros and subscriber
        rospy.init_node("game_controller")

        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        self.metrics = {
            'Status' : self.Status,
            'Time' : self.time_passed,
            'Correct': self.correct,
            'Score':self.score
        }

        

        self.git_dir = '/workspace/src/.git'
        commit_id = ''
        riders_project_id = os.environ.get('RIDERS_PROJECT_ID', '')

        # RIDERS_COMMIT_ID
        git_dir_exists = os.path.isdir(self.git_dir)
        if git_dir_exists:

            popen = subprocess.Popen(['git', 'remote', 'get-url', 'origin'], cwd=self.git_dir, stdout=subprocess.PIPE)
            remote_url, error_remote_url = popen.communicate()

            popen = subprocess.Popen(['git', 'name-rev', '--name-only', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            branch_name, error_branch_name = popen.communicate()

            popen = subprocess.Popen(['git', 'rev-parse', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            commit_id, error_commit_id = popen.communicate()

            if error_remote_url is None and remote_url is not None:
                remote_url = remote_url.rstrip('\n').rsplit('@')
                user_name = remote_url[0].rsplit(':')[1][2:]
                remote_name = remote_url[1]

        self.result_metrics = {
            'Status' : self.Status,
            'Correct' : self.correct,
            'Score': self.score,
            'Time' : self.time_passed,
            'commit_id': commit_id.rstrip('\n'),
        }


        self.init_services()

        self.send_api_check = False
        self.is_finish = False

        

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.robot_check = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = self.robot_check(self.robot_name, "")
            if not resp.success == True:
                print("no robot around")
                self.Status = "no_robot"

        except (rospy.ServiceException, rospy.ROSException):
            print("no robot around")
            self.Status = "no_robot"
            rospy.logerr("Service call failed: %s" % (e,))
        
        self.send_api_check = False


        while ~rospy.is_shutdown():
            
            self.time_passed = rospy.get_time() - self.start_time
            self.publish_metrics()
            self.state_msg = ModelState()        

            if(self.correct == 5): #?
                self.status = "Finished"
                self.result_metrics['Score'] = self.score * 1000
                self.result_metrics['Correct'] = self.correct
                self.result_metrics['Status'] = self.status
                self.result_metrics['Time'] = self.time_passed
                self.publish_metrics()
                self.send_to_api(self.score, False, **self.result_metrics)
                self.is_finish = True
            try:
                rate.sleep()
            except(rospy.exceptions.ROSTimeMovedBackwardsException), e:
                print(e)

            
            for i in self.model_names:
                try:
                    index = self.model_names.index(i)
                    
                    resp = self.get_robot_state(i, "")
                    diff_x1 = abs(resp.pose.position.x - self.finish_area1.x)
                    diff_y1 = abs(resp.pose.position.y - self.finish_area1.y)

                    diff_x2 = abs(resp.pose.position.x - self.finish_area2.x)
                    diff_y2 = abs(resp.pose.position.y - self.finish_area2.y)

                    if(diff_x1 <= 1.5 and diff_y1 <= 0.05) or (diff_x2 <= 0.3 and diff_y2 <= 0.25):  
                        self.check_point[index] = 1
                        
                        
                        self.score = self.correct /self.time_passed
                    
                    else:
                        self.check_point[index] = 0
                    
                    
                except (rospy.ServiceException, rospy.ROSException), e:
                    rospy.logerr("Service call failed: %s" % (e,))
            
            self.correct = 0
            for i in self.check_point:
                self.correct += i
                


    def init_services(self):
        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_robot_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
            
            resp = self.get_robot_state("legorider_organizer", "")
            if not resp.success == True:
                print("no robot")
            else:
                self.robot_new_position = Vector3D(resp.pose.position.x, resp.pose.position.y, resp.pose.position.z)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


    

    def publish_metrics(self):

        self.metrics['Status'] = self.Status
        self.metrics['Correct'] = str(self.correct)
        self.metrics['Score'] = int(self.score * 1000)
        self.metrics['Time'] = str(self.time_passed)     
        self.metrics['Block Counter'] = str(self.correct)

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))


    def send_to_api(self, time=0.0, disqualified=False,  **kwargs):
        simulation_id = os.environ.get('RIDERS_SIMULATION_ID', None)
        args = time, disqualified
        print("sent_to_api")
        try:
            if simulation_id:
                self.send_simulation_results(simulation_id, *args, **kwargs)
            else:
                self.send_branch_results(*args, **kwargs)
        except Exception as e:
            rospy.loginfo('Exception occurred while sending metric: %s', e)


    def send_simulation_results(self, simulation_id, time, disqualified, **kwargs):
        host = os.environ.get('RIDERS_HOST', None)
        token = os.environ.get('RIDERS_SIMULATION_AUTH_TOKEN', None)
        create_round_url = '%s/api/v1/simulation/%s/rounds/' % (host, simulation_id)
        kwargs['time'] = kwargs.get('time', time)
        kwargs['disqualified'] = kwargs.get('disqualified', disqualified)
        data = {
            'metrics': json.dumps(kwargs)
        }
        # send actual data
        requests.post(create_round_url, {}, data, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


    def send_branch_results(self, time, disqualified, **kwargs):
        rsl_host = 'http://localhost:8059'
        # metrics endpoint stores data differently compared to simulation endpoint,
        # hence we change how we send it
        info = {
            'time': time,
            'disqualified': disqualified,
        }
        info.update(kwargs)
        url = '%s/events' % rsl_host
        data = {
            'event': 'metrics',
            'info': json.dumps(info)
        }

        requests.post(url, {}, data, headers={
            'Content-Type': 'application/json',
        })

if __name__ == '__main__':
    # Name of robot
    controller = GameController("legorider_organizer")



   

